﻿using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour {

    const float locomotionAnimationSmoothTime = 0.1f;

    public AnimationClip[] defaultAttackAnimSet;
    public AnimationClip replaceableAttackAnim;

    protected Animator animator;
    protected CharacterCombat combact;
    protected AnimationClip[] currentAttackAnimSet;
    public AnimatorOverrideController overrideController;
    NavMeshAgent agent;

    protected virtual void Start() {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        combact = GetComponent<CharacterCombat>();

        // in this way we can swap animation clip with others
        if (overrideController == null) {
            overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        }
        animator.runtimeAnimatorController = overrideController;
        currentAttackAnimSet = defaultAttackAnimSet;

        combact.OnAttack += OnAttack;
    }

    protected virtual void Update() {
        // velocity of the player in percentage
        float speedPercent = agent.velocity.magnitude / agent.speed;
        // change the animation based on the velocity
        animator.SetFloat("speedPercent", speedPercent, locomotionAnimationSmoothTime, Time.deltaTime);
        animator.SetBool("inCombact", combact.InCombact);
    }

    protected virtual void OnAttack() {
        animator.SetTrigger("attack");
        int attackIndex = Random.Range(0, currentAttackAnimSet.Length);
        // swap the animation
        overrideController[replaceableAttackAnim.name] = currentAttackAnimSet[attackIndex];
    }
}
