﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour {

    public float attackSpeed = 1f;
    public float attackDelay = 0.6f;
    const float combactCooldown = 5f;

    float lastAttackTime;

    public bool InCombact { get; private set; }
    // notify with a delegate when player attacks
    public event Action OnAttack;

    // stats of the player
    CharacterStats myStats;
    CharacterStats opponentStats;
    float attackCooldown;

    void Start() {
        myStats = GetComponent<CharacterStats>();
    }

    void Update() {
        attackCooldown -= Time.deltaTime;

        // we are no longer in combact
        InCombact &= Time.time - lastAttackTime <= combactCooldown;
    }

    // attack the enemy with its stats target
    public void Attack(CharacterStats targetStats) {
        // enemy take damage
		if (attackCooldown <= 0f) {
            opponentStats = targetStats;
            if (OnAttack != null) {
                OnAttack();
            }
            // wait attack speed cooldown
            attackCooldown = 1f / attackSpeed;
            InCombact = true;
            lastAttackTime = Time.time;
		}
    }

    public void AttackHit_AnimationEvent() {
        opponentStats.TakeDamage(opponentStats.damage.GetValue());
        InCombact &= opponentStats.CurrentHealth <= 0;
    }
}
