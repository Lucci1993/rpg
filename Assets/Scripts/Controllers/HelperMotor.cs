﻿using UnityEngine.AI;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(NavMeshAgent))]
public class HelperMotor : MonoBehaviour {

    public Transform target;
    public float distance;
    public float radious;
    public LayerMask enemyMask;

    NavMeshAgent agent;
    bool isMoving;
    Transform currentTarget;

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        currentTarget = target;
    }

    void Update() {
        // if we have a target
        if (target != null && Vector3.Distance(transform.position, currentTarget.position) > distance) {
            isMoving = true;
            MoveToPoint(currentTarget.position);
            FaceTarget();
        }
        else if (isMoving && Vector3.Distance(transform.position, target.position) <= distance) {
            StopFollowingTarget();
            isMoving = false;
        }
        SearchEnemies();
    }

    // move to the point clicked
    public void MoveToPoint(Vector3 point) {
        // move to the point
        agent.SetDestination(point);
    }

    // stop move toward a target
    public void StopFollowingTarget() {
        agent.stoppingDistance = distance;
        agent.updateRotation = true;
    }

    // look the target in movement
    void FaceTarget() {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        // rotate until directly face target
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    // search and take the first enemy in range
    void SearchEnemies() {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radious, enemyMask);
        if (colliders.Length > 0) {
            currentTarget = colliders.First().transform;
        }
    }
}
