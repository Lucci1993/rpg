﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerAnimator : CharacterAnimator {

    public WeaponAnimations[] weaponAnimations;
    Dictionary<Equipment, AnimationClip[]> weaponAnimationsDict;

	protected override void Start() {
        base.Start();
        EquipmentManager.instance.onEquipmentChange += OnEquipmentChanged;

        weaponAnimationsDict = new Dictionary<Equipment, AnimationClip[]>();
        foreach (WeaponAnimations a in weaponAnimations) {
            weaponAnimationsDict.Add(a.weapon, a.clips);
        }
    }

    // when the equipment of the player change
	void OnEquipmentChanged(Equipment newItem, Equipment oldItem) {
        // when we equip a new weapon
        if (newItem != null && newItem.equipSlot == EquipmentSlot.Weapon) {
            animator.SetLayerWeight(1,1);
            // if contains the new weapon add the clips
            if (weaponAnimationsDict.ContainsKey(newItem)) {
                currentAttackAnimSet = weaponAnimationsDict[newItem];
            }
        }
        // when we unequip a weapon
        else if (newItem == null && oldItem != null && oldItem.equipSlot == EquipmentSlot.Weapon) {
            animator.SetLayerWeight(1,0);
            // came back to default
            currentAttackAnimSet = defaultAttackAnimSet;
        }
        // when we equip a new shield
        else if (newItem != null && newItem.equipSlot == EquipmentSlot.Shield) {
            animator.SetLayerWeight(2, 1);
        }
        // when we unequip a shield
        else if (newItem == null && oldItem != null && oldItem.equipSlot == EquipmentSlot.Shield) {
            animator.SetLayerWeight(2, 0);
        }
    }

    [Serializable]
    public struct WeaponAnimations {
        public Equipment weapon;
        public AnimationClip[] clips;
    }
}
