﻿using UnityEngine;
using UnityEngine.EventSystems; 

public class PlayerController : MonoBehaviour {

	// we move just in this zone
	public LayerMask movementMask;
	// what we have focus
	public Interactable focus;

	// reference for the camera
	private Camera cam;
	private PlayerMotor motor;

	private void Start () {
		cam = Camera.main;
		motor = GetComponent<PlayerMotor>();
	}
	
	// Update is called once per frame
	private void Update () {
        // check if we are hovering the UI
        if (EventSystem.current.IsPointerOverGameObject()) {
            return;
        }
        // if we press the left mouse button player will move in this position
        if (Input.GetMouseButtonDown(0)) {
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100f, movementMask)) {
				// move our player to what mouse hot
				motor.MoveToPoint(hit.point);
				// stop focusing on any object
				RemoveFocus();
			}
		}
		// if we press the left mouse button player will move in this position
		if (Input.GetMouseButtonDown(1)) {
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100f)) {
				// check if we hit an interactable, if we did set as our focus
				Interactable interactable = hit.collider.GetComponent<Interactable>();
				if (interactable != null) {
					SetFocus(interactable);
				}
			}
		}
	}
	
	// set the focus of the player on a interactable
 	private void SetFocus(Interactable newFocus) {
	    // if is not the same interactable
		if (newFocus != focus) {
			// if focus is not null
			if (focus != null) {
				focus.OnDefocused(); 
			}
			focus = newFocus;
			// move to the focus
			motor.FollowTarget(newFocus);
		}  
		// add the focus
		newFocus.OnFocused(transform);
 	}
	
	// set the focus of the player on a interactable
	private void RemoveFocus() {
		if (focus != null) {
			focus.OnDefocused();
		}
		focus = null;
		motor.StopFollowingTarget();
	}
}
