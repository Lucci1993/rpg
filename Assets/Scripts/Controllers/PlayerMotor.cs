﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerMotor : MonoBehaviour {

	private NavMeshAgent agent;
	private Transform target;

	private void Start () {
		agent = GetComponent<NavMeshAgent>();
	}

	private void Update() {
		// if we have a target
		if (target != null) {
			agent.SetDestination(target.position);
			FaceTarget();
		}
	}

	// move to the point clicked
	public void MoveToPoint(Vector3 point) {
		// move to the point
		agent.SetDestination(point);
	}
	
	// move toward a target
	public void FollowTarget(Interactable newTarget) {
		// until the radious of the target move
		agent.stoppingDistance = newTarget.radius * 0.8f;
		// follow the target with rotation
		agent.updateRotation = false;
		target = newTarget.interactionTransform;
	}
	
	// stop move toward a target
	public void StopFollowingTarget() {
		agent.stoppingDistance = 0f;
		agent.updateRotation = true;
		target = null;
	}

	// look the target in movement
	private void FaceTarget() {
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
		// rotate until directly face target
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
	}
}
