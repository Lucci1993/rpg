﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterStats))]
public class HealthUI : MonoBehaviour {

    public GameObject uiPrefab;
    public Transform target;
    float visibileTime = 5f;

    Transform ui;
    Image healthSlider;
    Transform cam;
    float lastVisibleTime;

	void Start () {
        cam = Camera.main.transform;
        foreach (Canvas c in FindObjectsOfType<Canvas>()) {
            // search the canvas in world space
            if (c.renderMode == RenderMode.WorldSpace) {
                ui = Instantiate(uiPrefab, c.transform).transform;
                healthSlider = ui.GetChild(0).GetComponent<Image>();
                ui.gameObject.SetActive(false);
                break;
            }
        }

        GetComponent<CharacterStats>().OnHealthChanged += OnHealthChanged;
	}

	void LateUpdate () {
        if (ui != null) {
            ui.position = target.position;
            // ui must be in the opposite direction of the camera
            ui.forward = -cam.forward;

            // after a while hide bar
            if (Time.time - lastVisibleTime > visibileTime) {
                ui.gameObject.SetActive(false);
            }
        }
	}

    // calculate the current health to give to the slider
    void OnHealthChanged(int maxHealth, int currentHealth) {
        if (ui != null) {
            ui.gameObject.SetActive(true);
            lastVisibleTime = Time.time;
            float healthPercent = currentHealth / (float)maxHealth;
            healthSlider.fillAmount = healthPercent;
            // if player is dead
            if (currentHealth <= 0) {
                Destroy(ui.gameObject);
            }
        }
    }
}
