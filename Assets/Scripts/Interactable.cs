﻿using UnityEngine;

public class Interactable : MonoBehaviour {

	// radious of action where inside player can interact with something
	public float radius = 3f;
	public Transform interactionTransform;
	
	// check if player is focusing the object
	private bool isFocus = false;
	private Transform player;
	// to not interact at the infinity
	private bool hasInteracted = false;

    private void Start() {
		// if the transform interaction is not setted, set to itself
		if (interactionTransform == null) {
			interactionTransform = transform;
		}
    }

    private void Update() {
		if (isFocus && !hasInteracted) {
            // if inside the radius
			if (Vector3.Distance(player.position, interactionTransform.position) <= radius) {
				// interact
				hasInteracted = true;
				Interact();
			}
		}
	}
	
	// interact with player
	public virtual void Interact() {
		// this method is meant to be overwritten
	}

	// set player that focused this object
	public void OnFocused(Transform playerTransform) {
		isFocus = true;
		player = playerTransform;
		// to make sure to interact once
		hasInteracted = false;
	}
	
	// defocus the object by player
	public void OnDefocused() {
		isFocus = false;
		player = null;
		// to make sure to interact once
		hasInteracted = false;
	}
}
