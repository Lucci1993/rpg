﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager instance;

    void Awake() {
        instance = this;
    }

    public GameObject player;

    // kill the player and reload the scene
    public void KillPlayer() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
