﻿using UnityEngine;

public class CharacterStats : MonoBehaviour {

    public int maxHealth = 100;
    public int CurrentHealth { get; private set; }
    public Stat damage;
    public Stat armor;
    public event System.Action<int, int> OnHealthChanged;

    void Awake() {
        CurrentHealth = maxHealth;
    }

    // take a damage from enemies
    public void TakeDamage(int damage) {

        damage -= armor.GetValue();
        // we don't want it negative
        damage = Mathf.Clamp(damage, 0, int.MaxValue);

        CurrentHealth -= damage;

        if (OnHealthChanged != null) {
            OnHealthChanged(maxHealth, CurrentHealth);
        }

        if (CurrentHealth <= 0) {
            Die();
        }
    }

    // this method is meant to be overwritten
    public virtual void Die() {
        // Die in some way
    }
}
