using UnityEngine;

public class PlayerStats : CharacterStats {

    void Start() {
        EquipmentManager.instance.onEquipmentChange += OnEquipmentChanged;
    }

    // set new equipment stats modifiers
    void OnEquipmentChanged(Equipment newItem, Equipment oldItem) {

        // add modifier of the new equipment
        if (newItem != null) {
            armor.AddModifier(newItem.armorModifier);
            damage.AddModifier(newItem.damageModifier);
        }

        // remove modifier of the old equipment
        if (oldItem != null) {
            armor.RemoveModifier(oldItem.armorModifier);
            damage.RemoveModifier(oldItem.damageModifier);
        }
    }

    // when player die
    public override void Die() {
        base.Die();
        // kill the player and restart the scene
        PlayerManager.instance.KillPlayer();
    }
}
